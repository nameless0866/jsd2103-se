package clooection;

import java.util.Deque;
import java.util.LinkedList;

public class StackDemo {
    public static void main(String[] args) {
        Deque<String> stack = new LinkedList<>();
        stack.push("one");
        stack.push("two");
        stack.push("three");
        stack.push("four");
        stack.push("five");
        System.out.println(stack);

        String e = stack.pop();
        System.out.println(e);
        System.out.println(stack);
        System.out.println("遍历");
        while(stack.size()>0){
            System.out.println(stack.pop());
        }
    }
}
