package clooection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * 集合的很多操作有与元素的equals方法相关。
 */
public class CollectionDemo2 {
    public static void main(String[] args) {
        //Collection c = new ArrayList();
        Collection c = new HashSet();
        c.add(new Point(1,2));
        c.add(new Point(3,4));
        c.add(new Point(5,6));
        c.add(new Point(7,8));
        c.add(new Point(7,8));
        System.out.println(c);

        Point p = new Point(1,2);
        boolean contains = c.contains(p);
        System.out.println("包含："+contains);
        c.remove(p);
        System.out.println(c);

    }

}
