package clooection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortListDemo2 {
    public static void main(String[] args) {
        List<Point> list = new ArrayList<>();
        list.add(new Point(1,2));
        list.add(new Point(8,9));
        list.add(new Point(7,6));
        list.add(new Point(12,15));
        list.add(new Point(5,4));
        list.add(new Point(3,2));
        System.out.println(list);
        //无法直接排序，因为Collections.sort 这个方法要求元素必须实现接口 Comparable接口，这个接口是用来规定实现类是可以比较
        //大小的，其中要求必须重写方法：compareTo用来定义该实例之间比较大小的规则。所以只有实现了该接口，sort方法才能利用元素中
        //compareTo方法在他们之间比较大小从而进行排序
        //Collections.sort(list);
        Collections.sort(list,  (o1, o2) ->
              o1.getX()*o1.getX() + o1.getY()*o1.getY()-
              o2.getX()*o2.getX() + o2.getY()*o2.getY()
        );
        System.out.println(list);
    }
}
