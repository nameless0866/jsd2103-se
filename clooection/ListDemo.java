package clooection;

import java.util.*;

/**
 * List集合
 * List是Collection下面常见的一类集合
 * java.util.List接口是所有List的接口，他继承自Collection
 * 常见的实现类：
 * java.util.ArrayList:内部由数据实现，查询性能更好
 * java.util.LinkedList:内部由链表实现，增删性能更好
 */
public class ListDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
//        List<String> list = new LinkedList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        String e = list.get(1);
        System.out.println(e);
        for (int i = 0; i < list.size(); i++) {
            e = list.get(i);
            System.out.println(e);
        }
        String old = list.set(1,"six");
        System.out.println(list);
        System.out.println("被替换的是"+old);

        for (int i = 0; i < list.size()/2 ; i++) {
            old = list.set(i,list.get(list.size()-1-i));
            list.set(list.size()-1-i,old);
        }
        Collections.reverse(list);
        System.out.println(list);
    }
}
