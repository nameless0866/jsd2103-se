package clooection;

import sun.invoke.empty.Empty;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Java集合框架
 * 集合与数组一样，可以存放一组元素，但是集合有多种不同的数据结构可供使用，将来我们可以更具不同数据结构的特点来保存一组元素并进行使用
 * Java。util。collection是所有集合的顶级接口，里面定义了所有集合都必须具备的相关功能
 * collection下面有两个创建的子接口
 * java.util.list:线性表，特点是可以存放重复元素，并且有序
 * java。util.set：不可以存放重复元素
 * 元素是否重复是依靠元素自身equals方法比较结果判定的
 */
public class CollectionDemo {
    public static void main(String[] args) {
        Collection c = new ArrayList();
        c.add("one");
        c.add("two");
        c.add("3");
        c.add("4");
        c.add("5");
        c.add("5");
/**
* boolean add(E e)
* 将给定元素添加到集合当中，元素成功添加后返回true
*/
        System.out.println(c);

        int size = c.size();
        System.out.println("size:"+size);

        boolean isEmpty = c.isEmpty();
        boolean d = (c == null);
        System.out.println(d);
        System.out.println(isEmpty);
        Collection c1 = new ArrayList();
        isEmpty = c1.isEmpty();
        System.out.println(isEmpty);
        d = (c1 == null);
        System.out.println(d);
        c.clear();
    }
}
