package clooection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CollectionToArrayDemo {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("1a");
        list.add("2b");
        list.add("3c");
        list.add("4d");
        list.add("5e");
        System.out.println(list);

//        Object[] array = list.toArray();
        String[] array = list.toArray(new String[8]);
        System.out.println(array.length);
        System.out.println(Arrays.toString(array));
    }
}
