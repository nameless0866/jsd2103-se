package clooection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Collection接口没有定义单独或去某一个元素的操作，因为不通用，但是Collection提供了遍历集合元素的操作，该操作是一个通用操作
 * 无论什么类型的集合都支持此种遍历方式：迭代器模式
 *
 * Inerator iterator （）
 * 该方法会获取一个用于遍历当前几何元素的迭代器
 *
 * Java。util。Iterator接口，是迭代器接口，规定了迭代器遍历集合的相关操作，不同的集合都提供了一个永不遍历自身元素的迭代器实现类，
 * 不过我们不需要知道他们的名字，以多态的方式当成Iterator使用即可
 * 迭代器遍历集合遵循的步骤为：问——》取——》删
 * 其中删除不是必须操作
 */

public class IteratorDemo {
    public static void main(String[] args) {
        Collection c = new ArrayList();
        c.add("one");
        c.add("#");
        c.add("two");
        c.add("#");
        c.add("three");
        c.add("#");
        c.add("four");
        c.add("#");
        c.add("five");
        System.out.println(c);
        Iterator it = c.iterator();
        while (it.hasNext()){
            String s = (String) it.next();
            System.out.println(s);
            System.out.println(it.next());
            if ("#".equals(s)){
                it.remove();
            }
        }
    }
}
