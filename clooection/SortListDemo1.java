package clooection;

import java.util.*;

public class SortListDemo1 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        Random ran = new Random();
        for (int i = 0; i < 10; i++) {
            list.add(ran.nextInt(100));
        }
        System.out.println(list);
        Collections.sort(list);
        System.out.println(list);
        Collections.sort(list,(o1,o2) -> o2-o1);
        System.out.println(list);
//        Collections.sort(list, new Comparator<Integer>() {
//            @Override
//            public int compare(Integer o1, Integer o2) {
//                if (o1%2 == o2%2){
//                    return o1-o2;
//                }else{
//                    return o1%2 - o2%2;
//                }
//            }
//        });
        Collections.sort(list,(o1, o2) -> o1%2 == o2%2 ? o1-o2 : o1%2 - o2%2);
        System.out.println(list);

    }
}
