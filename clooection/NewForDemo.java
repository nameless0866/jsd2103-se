package clooection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * JDK5推出时，推出了一个新的特性：增强型for循环
 * 也称为新循环，它可以用相同的语法遍历集合或数组。
 *
 * 新循环是java编译器认可的，并非虚拟机
 */

public class NewForDemo {
    public static void main(String[] args) {
        String[] a = {"a","b","c","d","e"};
        for (int i = 0; i < a.length; i++) {
            String s = a[i];
            System.out.println(s);
        }
        for (String s : a){
            System.out.println(s);
        }
/**
 * 泛型JDK5之后推出的另一个特性。
 * 泛型也称为参数化类型，允许我们在使用一个类时指定它里面属性的类型，
 * 方法参数或返回值的类型，使得我们使用一个类时可以更灵活。
 * 泛型被广泛应用于集合中，用来指定集合中的元素类型
 * 支持泛型的类在使用时如果未指定泛型，那么默认就是原型Object
 */
        Collection<String> c = new ArrayList<>();
        c.add("a1");
        c.add("b2");
        c.add("c3");
        c.add("d4");
        c.add("e5");
        Iterator<String> it = c.iterator();
        while(it.hasNext()){
            String str = it.next();
            System.out.println(str);
        }
        for (String s : c){
            System.out.println(s);
        }
    }
}
