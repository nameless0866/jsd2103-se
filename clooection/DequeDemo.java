package clooection;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 双端队列
 * java.util.Deque端口，双端队列是队列两端都可以做出入队的队列
 * Deque继承自Queue,常用实现类：linkedList
 */
public class DequeDemo {
    public static void main(String[] args) {
        Deque<String > deque = new LinkedList<>();
        deque.offer("one");
        deque.offer("two");
        deque.offer("three");
        System.out.println(deque);

        deque.offerFirst("four");
        deque.offerLast("five");
        System.out.println(deque);
    }
}
