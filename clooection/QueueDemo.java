package clooection;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 队列java.util.Queue
 * 队列Queue接口继承自Collection. 因此队列可以保存一组元素，但是存取元素必须遵循先进先出原则。
 * FIFO：First Input First Output
 *
 * 常见实现类：LinkedList
 */
public class QueueDemo {
    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();
        queue.offer("one");
        queue.offer("two");
        queue.offer("three");
        queue.offer("four");
        queue.offer("five");
        queue.offer("six");
        //poll 是出队操作，获取并删除了队首元素
        System.out.println(queue);
        String str = queue.poll();
        System.out.println(str);
        System.out.println(queue);
        //peek方法是引用队首，获取后元素还在队列中
        str = queue.peek();
        System.out.println(str);
        System.out.println(queue);
        System.out.println(queue.size());
        for (String e : queue){
            System.out.println(e);
        }
        System.out.println(queue);
        while (queue.size()>0){
            System.out.println(queue.poll());
        }
        System.out.println(queue);
    }
}
