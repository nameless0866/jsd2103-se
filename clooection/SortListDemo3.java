package clooection;

import java.util.*;

/**
 * 排序字符串
 *
 * 当即和元素已经实现了Comparable接口，但是该比较规则不满足我们排序需求时，也可以传入一个比较器用自定义比较规则进行排序
 *
 *
 */
public class SortListDemo3 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("高宇");
        list.add("王硕");
        list.add("田秋实老师");
        list.add("崔雷");
        list.add("王琦");
        list.add("邰佳琪");
        list.add("孙雨楠");
        System.out.println(list);
//        Collections.sort(list,(o1, o2) -> o1.length()- o2.length());
        Collections.sort(list, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return o2.length()-o1.length();
            }
        });
        System.out.println(list);

    }
}
