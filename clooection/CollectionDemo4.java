package clooection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class CollectionDemo4 {
    public static void main(String[] args) {
        Collection c1 = new HashSet();
        c1.add("java");
        c1.add("C");
        c1.add("C++");

        Collection c2 = new ArrayList();
        c2.add("amzhuo");
        c2.add("pingguo");
        c1.add("C++");
        //c1.add(c2);
        c1.addAll(c2);
        System.out.println(c1);
        boolean tf = c1.addAll(c2);
        System.out.println(tf);

        Collection c3 = new ArrayList();
        c3.add("pingguo");
        c3.add("c++");
        boolean b = c1.contains(c3);
        System.out.println(b);
        System.out.println(c1);
        System.out.println(c3);
        c1.removeAll(c3);
        System.out.println(c1);
        System.out.println(c3);
    }
}
