package clooection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayToListDemo {
    public static void main(String[] args) {
        String[] array = {"1a","2b","3c","4d","5e"};
        System.out.println(Arrays.toString(array));
        List<String> l = Arrays.asList(array);
        System.out.println(l);
        l.set(1,"6f");
        System.out.println(l);
        System.out.println(Arrays.toString(array));

        List<String> list2 = new ArrayList<>(l);
        System.out.println(list2);
        list2.add("6f");
        System.out.println(list2);

    }
}
