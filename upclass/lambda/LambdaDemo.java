package upclass.lambda;

import java.io.File;
import java.io.FileFilter;

public class LambdaDemo {
    public static void main(String[] args) {
        FileFilter filter = new FileFilter() {
            public boolean accept(File file) {
                return file.getName().startsWith(".");
            }
        };
        FileFilter filter2 = (File file) ->{
            return file.getName().startsWith(".");
        };
        FileFilter filter3 = (File file) -> file.getName().startsWith(".");

    }
}
