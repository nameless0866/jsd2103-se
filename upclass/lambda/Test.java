package upclass.lambda;

import java.io.File;
import java.io.FileFilter;

public class Test {
    public static void main(String[] args) {
        File dir = new File(".");
        if (dir.isDirectory()) {
//        File[] subs = dir.listFiles( (file) -> file.getName().indexOf("a")>=0);
            File[] subs = dir.listFiles((file) -> file.getName().contains("a"));
            for (int i = 0; i < subs.length; i++) {
                System.out.println(subs[i].getName());
            }
        }
    }
}
