package upclass.day01.api;

/**
 * String toUpperCase()
 * String toLowercase()
 */
public class ToUpperCaseDemo {
    public static void main(String[] args) {
        String line = "我爱Java";
        String lower = line.toLowerCase();
        System.out.println(lower);
        String upper = line.toUpperCase();
        System.out.println(upper);
    }
}
