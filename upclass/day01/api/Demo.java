package upclass.day01.api;

/**
 * 文档注释是功能及注释，用来说明一个类，一个方法或一个常量的，因此只在上述三个地方使用。
 * 文档注释可使用Java自带的命令javadoc来对这个类生成手册
 * 在类上使用时用来说明当前类的整体功能
 * @author NAMELESS
 */
public class Demo {
    /**
     * sayHello 中使用的问候语
     */
    public static final String INFO = "Hello";

    /**
     *为给定的用户添加一个问候语
     * @param name  指定的用户的名字
     * @return      返回了含有问候语的字符串；
     */
    public String sayHello(String name){
        return  "Hello!"+name;
    }
}
