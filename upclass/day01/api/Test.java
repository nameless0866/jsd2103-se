package upclass.day01.api;

public class Test {
    public static void main(String[] args) {
        String name1 = getHostName("www.tedu.cn");
        String name2 = getHostName("http://www.canglaoshi.org");
        String name3 = getHostName("www.tarena.com.cn");
        System.out.println(name1);
        System.out.println(name2);
        System.out.println(name3);
    }
    public static String getHostName(String line){
        int a = line.indexOf(".")+1;
        int b = line.indexOf(".",a);
        String c = line.substring(a,b);
        return c;
    }
}
