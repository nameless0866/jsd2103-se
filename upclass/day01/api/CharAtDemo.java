package upclass.day01.api;

public class CharAtDemo {
    public static void main(String[] args) {
        //            0123456789012345
        String str = "thinking in java";
        char c = str.charAt(10);
        System.out.println(c);

        boolean check = true;
        String line = "明天到操场操到天明";
        for (int i = 0 ; i < line.length()/2 ; i++){
            char c1 = line.charAt(i);
            char c2 = line.charAt(line.length() - 1 - i);
            if(c1 != c2){
                System.out.println("不是回文");
                check = false;
                break;
            }

        }
        if (check){
            System.out.println("是回文");
        }
    }
}
