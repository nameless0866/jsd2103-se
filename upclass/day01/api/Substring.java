package upclass.day01.api;

public class Substring {
    public static void main(String[] args) {
        String line = "www.tedu.cn";
        String str = line.substring(4,8);
        System.out.println(str);
        str = line.substring(4);
        System.out.println(str);
        str = line.substring(0,8);
        System.out.println(str);
    }
}
