package upclass.day01.api;

public class StartsWithDemo {
    public static void main(String[] args) {
        String line = "http://www.tedu.com";
        boolean a = line.startsWith("http");
        System.out.println(a);
        boolean b = line.endsWith(".com");
        System.out.println(b);
    }
}
