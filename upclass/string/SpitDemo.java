package upclass.string;

import java.util.Arrays;

public class SpitDemo {
    public static void main(String[] args) {
        String str = "abc123def456ghi";
        String[] data = str.split("[0-9]+");
        System.out.println(data.length);
        System.out.println(Arrays.toString(data));
        str= "abc,1def,1ghi,1jkl";
        String[] data1 = str.split(",");
        System.out.println(data1.length);
        System.out.println(Arrays.toString(data1));
        str =  "abc.1def.1ghi.1jkl";
        data = str.split("\\.");
        System.out.println(data.length);
        System.out.println(Arrays.toString(data));
        str = ".abc.1def.1ghi.1jkl..1..................";
        data = str.split("\\.");
        System.out.println(data.length);
        System.out.println(Arrays.toString(data));

    }
}
