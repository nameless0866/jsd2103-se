package upclass.string;

public class ReplaceAllDemo {
    public static void main(String[] args) {
        String str = "abc123def456ghi";
        str = str.replaceAll("[0-9]+","#MUMBER#");
        System.out.println(str);
        str = "abc123def456ghi";
        str = str.replaceAll("[0-9]","#MUMBER#");
        System.out.println(str);
        String regex = "(wqnmlgb|cnm|dsb|nc|mdzz|nmsl|wcngnm|djb)";
        String message = "wqnmlgb,你个dsb,你这么nc,nmsl";
        message = message.toLowerCase();
        message = message.replaceAll(regex,"我爱你");
        System.out.println(message);
    }
}
