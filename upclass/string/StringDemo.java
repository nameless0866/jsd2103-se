package upclass.string;

/**
 * 字符串String
 * 内部使用一个char数组保存所有在字符，每个字符为2字节，存的是该字符的unicode编码。
 * 字符串是不变对象，一旦创建内容无法被改变，若改变会创建新对象
 */
public class StringDemo {
    public static void main(String[] args) {
        /*
            字符创常量池
            java在堆内存中开辟了一段空间用于缓存所有使用字面量形式创建的字符串对象，
            并在后期再次使用该字面量穿件字符串时重用对象，避免内存中堆积大量内容一样的字符串对象来减小内存开销
         */
        String s1 = "123abc";
        String s2 = "123abc";
        //地址相同，说明s2重用了s1对象
        System.out.println(s1 == s2);
        String s4 = new String("123abc");
        System.out.println("s4:"+s4);
        System.out.println(s1 == s4);
        s1 = s1 + "!";
        s2 = s2 + "!";
        String s3 = "123"+"abc";
        int a = 86400000;
        int b = 60 * 60 * 24 * 1000;
        System.out.println(a == b);
        /*
            通常我们判断字符串都是比较内容，因此应当使用字符串的equals方法
         */
        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));
        /*
            这里触发了一个编译器的特性：
            编译器在编译期间若遇到几个计算表达式，发现在编译器可以确定结果时就会进行计算并将结果编译到class文件中，
            这样一来JVM每次执行字节码文件就无需再计算了
            下面的代码会被编译器改为：
            String s5 = "123abc"
            也因此s5会从用常量池中的对象，所以地址与s2相同
         */
    }
}
