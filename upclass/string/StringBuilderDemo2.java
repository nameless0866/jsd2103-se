package upclass.string;

public class StringBuilderDemo2 {
    public static void main(String[] args) {
        System.out.println("开始");
        StringBuilder sb = new StringBuilder("a");
        for (int i = 0; i < 100000000; i++) {
            sb.append("a");
        }
        System.out.println(sb);
        StringBuffer sb1 = new StringBuffer();
        for (int i = 0; i < 100000000; i++) {
            sb1.append("a");
        }
        System.out.println("执行完毕");
    }
}

/**
 * 正则表达式：
 * ^[abc]{3}$
 * ^[abc].*[abc]$
 */
