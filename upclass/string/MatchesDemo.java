package upclass.string;

public class MatchesDemo {
    public static void main(String[] args) {
        /*
            [a-zA-Z0-9_]+ @[a-zA-Z0-9]+ (\.[a-zA-Z]+)+
         */
        String mail = "baidu@163.com";
        String regex = "[a-zA-Z0-9_]+@[a-zA-Z0-9]+(\\.[a-zA-Z]+)+";

        boolean match = mail.matches(regex);
        if(match){
            System.out.println("是邮箱");
        }else{
            System.out.println("不是邮箱");
        }
    }
}
