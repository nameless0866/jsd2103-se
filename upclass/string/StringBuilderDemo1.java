package upclass.string;

public class StringBuilderDemo1 {
    public static void main(String[] args) {
        String line ="好学JAVA";
        StringBuilder sb = new StringBuilder(line);
        sb.append(",为了找个好工作");
        System.out.println(line);
        System.out.println(sb);
        sb.replace(6,sb.length(),"是为了改变世界!");
        System.out.println(sb);
        sb.delete(0,6);
        System.out.println(sb);
        sb.insert(0,"活着,");
        System.out.println(sb);
        sb.reverse();
        System.out.println(sb);
        line = sb.toString();
        System.out.println(line);
/*        String a = "a";
        StringBuilder sb1 = new StringBuilder();
        for (int i = 0; i < 10000000; i++) {
            sb1.append("a");
        }
        System.out.println("运行完毕");
        sb.replace()

 */
    }
}
