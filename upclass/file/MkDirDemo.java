package upclass.file;

import java.io.File;

public class MkDirDemo {
    public static void main(String[] args) {
        File dir = new File("./a/b/c/g/e/f/g");
        if(dir.exists()){
            System.out.println("目录已存在");
        }else {
            dir.mkdirs();
            System.out.println("创建成功");
        }
    }
}
