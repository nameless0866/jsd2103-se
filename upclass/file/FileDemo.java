package upclass.file;

import java.io.File;

public class FileDemo {
    public static void main(String[] args) {
        File file = new File("./demo.txt");
        String name = file.getName();
        System.out.println(name);
        long len = file.length();
        System.out.println(len+"字节");
        boolean cr = file.canRead();
        boolean cw = file.canWrite();
        System.out.println("是否可读"+cr);
        System.out.println("是否可写"+cw);
    }
}
