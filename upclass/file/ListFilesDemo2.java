package upclass.file;

import java.io.File;
import java.io.FileFilter;

public class ListFilesDemo2 {
    public static void main(String[] args) {
        File dir = new File(".");
        if (dir.isDirectory()){
//            FileFilter filter = new FileFilter() {
//                @Override
//                public boolean accept(File file) {
//                    String name = file.getName();
//                    boolean starts =name.startsWith(".");
//                    System.out.println("过滤器"+name+"，是否符合要求"+starts);
//                    return starts;
//                }
//            };
//            File[] subs = dir.listFiles(filter);
            File[] subs = dir.listFiles(new FileFilter(){
               public boolean accept(File file){
                   return file.getName().startsWith(".");
               }
            });
            System.out.println(subs.length);
        }
    }
}
