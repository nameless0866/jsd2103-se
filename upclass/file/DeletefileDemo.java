package upclass.file;

import java.io.File;

public class DeletefileDemo {
    public static void main(String[] args) {
        File file = new File("test.txt");
        if(file.exists()){
            file.delete();
            System.out.println("文件已删除");
        }else{
            System.out.println("文件不存在");
        }
    }
}
