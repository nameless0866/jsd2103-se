package upclass.file;

import java.io.File;

public class Test {
    public static void main(String[] args) {
        File dir = new File("a");
        delete(dir);
        System.out.println("删除完毕");
    }
    public static void delete(File f){
        if(f.isDirectory()){
            File[] subs = f.listFiles();
            for (int i = 0; i < subs.length;i++){
                File sub = subs[i];
                delete(sub);
            }
        }
        f.delete();
    }
}
