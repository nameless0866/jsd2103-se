package upclass.exception;

public class Person {
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws Exception {
        this.age = age;
        if(age < 0 || age > 100){
            //使用throw 对外抛出一个异常
//            throw new RuntimeException("年龄不合法");
//            throw new Exception("年龄不合法");
            throw new IllegalAgeException("年龄范围" + age);
        }
    }
}
