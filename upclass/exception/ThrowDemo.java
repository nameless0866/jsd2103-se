package upclass.exception;

public class ThrowDemo {
    public static void main(String[] args) {
        try {
            System.out.println("程序开始");
            /*
                当我们调用一个含有throws声明异常抛出的方法时，编译器要求我们必须添加处理异常的手段，否则编译器不通过，而处理手段有两种
                1：使用try-catch捕获并处理异常
                2：在当前方法上继续使用throws声明该异常的抛出
                具体用哪种取决于异常处理的责任问题
            */
            Person p = new Person();
            p.setAge(10000);
            System.out.println("此人年龄："+p.getAge()+"岁");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("程序结束");
    }
}
