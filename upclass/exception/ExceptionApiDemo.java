package upclass.exception;

public class ExceptionApiDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了");

        try {
            String str = "abc";
            System.out.println(Integer.parseInt(str));
        } catch (NumberFormatException e) {
            //异常最常用的方法，用于将当前错误信息输出到控制台
            e.printStackTrace();

            //获取错误信息，记录日志的时候或提示给用户可以使用它
            String message = e.getMessage();
            System.out.println(message);
        }

        System.out.println("程序结束啦");
    }
}
