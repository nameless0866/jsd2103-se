package upclass.exception;

/**
 * 和异常相关的面试问题
 * 请分别说出final finally finalize
 *
 * finalize 是Object 中定义的一个方法，他是一个对象生命周期中最后一个被调用的方法，
 * 当一个对象即将被GC回收前，GC会调用该对象的finalize方法，该方法调用完毕后就会被
 * 释放.API手册对该方法有明确说明，此方法不能有耗时的操作，否则会影响GC工作
 */
public class FinallyDemo3 {
}
