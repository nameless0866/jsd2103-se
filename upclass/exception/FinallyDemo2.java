package upclass.exception;

import java.io.*;

public class FinallyDemo2 {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = null;
        try{
            fos = new FileOutputStream("fox.dat");
            fos.write(1);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try{
                if(fos != null){
                    fos.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
