package upclass.exception;

public class FinallyDemo {
    public static void main(String[] args) {
        try{
            String str = "null";
            System.out.println(str.length());
        }catch(Exception e){
            System.out.println("出错了");
        }finally {
            System.out.println("finally中的代码执行了！");
        }
        System.out.println("运行结束");
    }
}
