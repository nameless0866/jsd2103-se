package upclass.exception;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

public class ThrowsDemo {
    public void doSome()throws IOException, AWTException{}
    //public void doSome(){}
    //public void doSome()throws IOException{}

}
class SubClass extends ThrowDemo{

    //public void doSome()throws IOException, AWTException{}
    //public void doSome()throws IOException{}
    //public void doSome(){}
    //可以抛出超类方法抛出异常的子类异常
    //public void doSome()throws FileNotFoundException {}
    //不允许抛出额外异常（超类里没有且没有继承的异常）
    //public void doSome()throws SQLException{}
    //不可以抛出超类方法抛出异常的超类型异常
    //public void doSome()throws Exception{}
}
