package upclass.exception;

public class TryCatchDemo {
    public static void main(String[] args) {
        try {
            System.out.println("开始");
            String str = "a";
            System.out.println(str.length());
            System.out.println(str.charAt(0));
            System.out.println(Integer.parseInt(str));
        }catch(NullPointerException e){
            System.out.println("出现了空指针！");
        }catch(StringIndexOutOfBoundsException e){
            System.out.println("出现了下标越界！");
        }catch(Exception e){
            System.out.println("不到咋了 反正就是出错了");
        }
        System.out.println("结束");
    }
}
