package upclass.io;

import java.io.*;

public class PWDemo2 {
    public static void main(String[] args) throws UnsupportedEncodingException, FileNotFoundException {
        FileOutputStream fos = new FileOutputStream("pw.txt");
        OutputStreamWriter osw = new OutputStreamWriter(fos,"utf-8");
        BufferedWriter bw = new BufferedWriter(osw);
        PrintWriter pw = new PrintWriter(bw);
        pw.println("所以");
        pw.println("你好");
        pw.println("，");
        pw.println("再见");
        pw.println("goodbye");
        pw.println("撒有哪啦");
        System.out.println("写出完毕");
        pw.close();
    }
}
