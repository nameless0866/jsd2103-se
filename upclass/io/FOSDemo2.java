package upclass.io;

import java.io.FileOutputStream;
import java.io.IOException;

public class FOSDemo2 {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("fos.txt",false);
        fos.write(97);
        fos.write(98);
        fos.write(99);
        System.out.println("写出完毕");
        fos.close();
    }
}
