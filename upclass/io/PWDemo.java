package upclass.io;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class PWDemo {
    public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter pw = new PrintWriter("pw.txt","utf-8");
        pw.println("窗外的麻雀");
        pw.println("在电线杆上多嘴");
        pw.println("你说这一句");
        pw.println("很有夏天的感觉");
        pw.println("手中的铅笔");
        pw.println("在纸上来来回回");
        pw.println("我用几行字形容你是我的谁");
        pw.println("秋刀鱼 得滋味");
        pw.println("猫和你 都想了解");
        pw.println("初恋的感觉就这样被我们找回");
        pw.println("那温暖的阳光");
        pw.println("·");
        pw.println("·");
        pw.println("雨下整夜");
        pw.println("我的爱溢出就像雨水");
        pw.println("院子落叶");
        pw.println("跟我的思念厚厚一叠");
        pw.println("几句是非");
        pw.println("也无法将我的热情冷却");
        pw.println("你出现在我诗的每一页");
        System.out.println("写出完毕");
        pw.close();
    }
}
