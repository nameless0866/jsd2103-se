package upclass.io;

import java.io.*;

public class ISRDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("osw.txt");
        InputStreamReader isr = new InputStreamReader(fis,"utf-8");
        int d;
        while ((d = isr.read()) != -1){
            System.out.print((char)d);
        }
        isr.close();
    }
}
