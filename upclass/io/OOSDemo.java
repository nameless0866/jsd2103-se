package upclass.io;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class OOSDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("person.obj");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        String name = "田秋实";
        int age = 24;
        String gender = "男";
        String[] otherInfo = {"是我的组员","嘴非常碎","长得挺精神","是组内吉祥物","坐我后面"};
        Person p = new Person(name,age,gender,otherInfo);
        System.out.println(p);

        oos.writeObject(p);
        System.out.println("写出完毕");
    }


}
