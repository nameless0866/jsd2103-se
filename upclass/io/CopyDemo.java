package upclass.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream file = new FileInputStream("demo.txt");
        FileOutputStream file1 = new FileOutputStream("demo1.txt");
        int d;
        while ((d = file.read()) != -1 ){
            file1.write(d);
        }
        System.out.println("复制完毕");
        file.close();
        file1.close();
    }
}
