package upclass.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyDemo2 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("SteamSetup.exe");
        FileOutputStream fos = new FileOutputStream("SteamSetup1.exe");
        int len;
        long start = System.currentTimeMillis();
        byte[] data = new byte[1024*10];
        while((len = fis.read(data)) != -1){
            fos.write(data,0,len);
        }
        fos.close();
        fis.close();
        long end = System.currentTimeMillis();
        System.out.println("写入完毕");
        System.out.println("耗时"+(end-start)+"毫秒");
    }
}
