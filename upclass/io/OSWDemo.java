package upclass.io;

import java.io.*;

public class OSWDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("osw.txt");
        OutputStreamWriter osw = new OutputStreamWriter(fos,"utf-8");
        osw.write("让我在看你一眼，");
        osw.write("从南到北；");
        osw.write("像是被五环路，");
        osw.write("蒙住的双眼；");
        osw.write("请你再看一遍，");
        osw.write("关于那天；");
        osw.write("你回家了，");
        osw.write("我在等你呢");
        System.out.println("写出完毕");
        osw.close();
    }
}
