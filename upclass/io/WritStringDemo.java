package upclass.io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class WritStringDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos = new FileOutputStream("fos.txt");
        String line = "万丈高楼平地起，辉煌只能靠自己";
        byte[] data = line.getBytes("UTF-8");
        fos.write(data);
        line = "aaaa";
        data = line.getBytes("UTF-8");
        fos.write(data);
        System.out.println("写出完毕！");

    }
}
