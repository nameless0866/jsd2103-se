package upclass.io;

import java.io.FileOutputStream;
import java.io.IOException;

public class FOSDemo1 {
    public static void main(String[] args) throws IOException {
//        File file = new File("./fos.dat");
//        FileOutputStream fos = new FileOutputStream(file);
        FileOutputStream fos = new FileOutputStream("./fos.dat");
        System.out.println("文件创建完毕");
        fos.write(1);
        fos.write(2);
        fos.close();
    }
}
