package upclass.io;

import java.io.FileInputStream;
import java.io.IOException;

public class FISDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("fos.dat");
        int d = fis.read();
        int d2 = fis.read();
        int d3 = fis.read();
        System.out.println(d);
        System.out.println(d2);
        System.out.println(d3);
    }
}
