package upclass.io;

import java.io.*;

public class CopyDemo3 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis = new FileInputStream("SteamSetup.exe");
        BufferedInputStream bis = new BufferedInputStream(fis);
        FileOutputStream fos = new FileOutputStream("SteamSetup1.exe");
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        int d;
//        while((d = fis.read()) != -1){
//            bos.write(d);
//        }
//        System.out.println("复制完毕");
//        bis.close();
//        bos.close();
        byte[] date = new byte[1024*8];
        while ((d = bis.read(date)) != -1){
            bos.write(d);
        }
        System.out.println("输出完毕");
        bos.close();
        bis.close();
    }
}
