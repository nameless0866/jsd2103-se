package upclass.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.*;
import java.util.Scanner;

public class NoteDemo {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入一个文件名");
        String name = scan.nextLine();
//        FileOutputStream fos = new FileOutputStream(name);
//        String b;
//        while(true){
//            System.out.println("请输入输入内容");
//            b = scan.nextLine();
//            if("exit".equals(b)){
//                break;
//            }
//            byte[] bt = b.getBytes("utf-8");
//            fos.write(bt);
//        }
//        System.out.println("再见");
//        fos.close();
        FileOutputStream fos = new FileOutputStream(name+".txt");
        OutputStreamWriter osw = new OutputStreamWriter(fos,"utf-8");
        BufferedWriter bw = new BufferedWriter(osw);
        PrintWriter pw = new PrintWriter(bw);
        String b;
        System.out.println("请输入笔记内容，输入exit退出");
        while (true){
            b = scan.nextLine();
            if("exit".equals(b)){
                System.out.println("写出完毕");
                break;
            }
            pw.println(b);
        }
        pw.close();
    }
}
