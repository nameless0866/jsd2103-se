package upclass.day02.integer;

public class IntegerDemo2 {
    public static void main(String[] args) {
        int imax = Integer.MAX_VALUE;
        System.out.println(imax);
        int imin = Integer.MIN_VALUE;
        System.out.println(imin);

        long lmax = Long.MAX_VALUE;
        long lmin = Long.MIN_VALUE;
        System.out.println(lmax);
        System.out.println(lmin);

        Double dmax = Double.MAX_VALUE;
        Double dmin = Double.MIN_VALUE;
        System.out.println(dmax);
        System.out.println(dmax);

        Character cmax = Character.MAX_VALUE;
        Character cmin = Character.MIN_VALUE;
        System.out.println(cmax);
        System.out.println(cmin);

        String s = "132";
        int a = Integer.parseInt(s);
        double b = Double.parseDouble(s);

    }
}
