package upclass.day02.integer;

public class IntegerDemo1 {
    public static void main(String[] args) {
        int i =123;
//        Integer i1 = new Integer(i);
        Integer i1 = Integer.valueOf(i);
        Integer i2 = Integer.valueOf(i);
        System.out.println(i1 == i2);
        System.out.println(i1.equals(i2));
        Character c = Character.valueOf((char)i);
        System.out.println(c);
        Double d1 = Double.valueOf(321.123);
        Double d2 = Double.valueOf(321.123);
        System.out.println(d1==d2);
        System.out.println(d1.equals(d2));
        int ii = i1.intValue();
        double dd = i1.intValue();
        ii = d1.intValue();
        dd = d1.doubleValue();
        System.out.println(ii);
        System.out.println(dd);
//        dd = ii.intValue();
    }

}
