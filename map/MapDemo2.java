package map;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Map的遍历
 * Map提供了三种遍历方式
 * 1：遍历所有的key
 * 2：遍历每一组键值对
 * 3：遍历所有的value（不常用）
 */
public class MapDemo2 {
    public static void main(String[] args) {
        Map<String , Integer> map = new HashMap<>();
        map.put("英语",15);
        map.put("数学",115);
        map.put("语文",80);
        map.put("化学",70);
        map.put("物理",50);

        /*
            遍历所有的key
            set keyset（）
            将当前map中所有的key以一个Set集合形式返回。遍历该集合就等同于便利了所有得key

         */
        Set<String > keySet = map.keySet();
        for(String key : keySet){
            System.out.println("key:"+key);
        }
        /*
            遍历每一组键值对
            Set<Entry> entrySet()
            将当前Map中每一组键值对以一个Entry实例形式存入Set集合后返回。
            java.util.Map.Entry
            Entry的每一个实例用于表示map中的一组键值对，其中有两个常用方法
            getKey() 及 getValue()
         */
//        Set<Map.Entry<String , Integer>> entrySet = map.entrySet();
//        for(Map.Entry<String , Integer> e : entrySet){
//            String key = e.getKey();
//            Integer value = e.getValue();
//            System.out.println(key+":"+value);
//        }
        /*
            JDK8之后集合框架支持了使用lambda表达式遍历。因此Map和Collection 都提供了foreach方法 通过lambda表达式遍历元素
         */
        map.forEach(
                (k,v)-> System.out.println(k+":"+v)
        );
        /*
            遍历所有的value
            Collection values
            将当前map中所有的value以一个集合形式返回
         */
        Collection<Integer> values = map.values();
        System.out.println(values);
        for(Integer i : values){
            System.out.println("value:"+i);
        }
        values.forEach(i -> System.out.println(i));

    }
}

