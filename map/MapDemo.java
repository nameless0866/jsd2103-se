package map;

import java.util.HashMap;
import java.util.Map;

/**
 * java.util.map接口 查找表
 * map体现的结构像是一个多行两列的表格，其中左列称为key，右列称为value
 * map总是成对儿（key-value键值对）保存数据，并且总是根据key获取其对应的value
 *
 * 常用实现类
 * java.util.HashMap: 称为散列表，使用散列算法实现的map 单进查询速度最快的数据结构
 */
public class MapDemo {
    public static void main(String[] args) {
        Map<String,Integer> map = new HashMap<>();
        /*
            V put(K k,V v)
            将给定的键值对存入map
            map有一个要求，即：key不允许重复（Key的equals比较）
            因此如果使用重复的key存入value，则是替换value操作，此时put方法的返回值就是被替换的value。否则返回值为null
         */

        Integer value = map.put("语文",80);
        System.out.println(value);
        map.put("数学",115);
        map.put("英语",60);
        map.put("物理",50);
        map.put("化学",70);
        System.out.println(map);
        value = map.put("语文",44);
        System.out.println(value);
        System.out.println(map);
        /*
            V get(object key)
            根据给定的key获取对应的value。若给定的key不存在则返回值为null
         */
        value = map.get("语文");
        System.out.println("语文"+value);
        value = map.get("体育");
        System.out.println(value);

        int size = map.size();
        System.out.println("size:"+size);

        /*
            V remove(object key)
            删除给定的key所对应的键值对，返回值为这个key对应得value
         */
        value = map.remove("语文");
        System.out.println(map);
        System.out.println(value);
        /*
            boolean containsKey(Object key)
            判断当前map是否包含给定的key
            boolean containsvalue(Object key)
            判断当前map是否包含给定的value
         */
        boolean ck = map.containsKey("英语");
        System.out.println("包含key"+ck);
        boolean cv = map.containsValue(115);
        System.out.println("包含value"+cv);
    }
}
