package socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * 聊天室服务端
 */
public class Server {
    /**
     * 运行在服务端的ServerSocket主要完成两个工作：
     * 1：向服务端操作系统申请服务端口，客户端就是通过这个端口与ServerSocket建立连接
     * 2：监听端口,一旦一个客户端建立连接，会立刻返回一个Socket。通过这个Socket就可以和该客户端交互了
     * <p>
     * 我们可以把ServerSocket想象成某客服的“总机”。用户打电话到总机，总机分配一个电话使得服务端与你沟通。
     */
    private ServerSocket serverSocket;

//    private PrintWriter[] allOut = {};
    private Collection<PrintWriter> allOut = new ArrayList<>();
    public Server() {
        try {
            System.out.println("正在启动服务器");
            /*
                实例化ServerSocket时要指定服务端口，该端口不能与操作系统其他应用程序占用程序占用的端口相同，
                负责会抛出异常：
                java.net.BindException:address already in use

                端口是一个数字，取值范围0-65535之间。
                6000之前的端口不要使用，密集绑定系统应用和流行应用程序。
             */
            serverSocket = new ServerSocket(8448);
            System.out.println("服务器启动完毕");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start() {
        try {
            while (true) {
                System.out.println("等待客户端连接。。。");
            /*
                ServerSocket提供了接收客户端连接的方法：
                Socket accept()
                这个方法是一个阻塞方法，调用后方法“卡主”，此时开始等待客户端的连接
                ，知道一个客户端连接，此时该方法会立即返回一个Socket实例
                通过这个Socket就可以与客户端进行交互了

                可以理解为此操作是接电话，电话没响时就一直等。
             */

                Socket socket = serverSocket.accept();
                System.out.println("一个客户端连接了");

            /*
                Socket提供的按方法：
                InputStream getInputStream()
                获取的字节输入流读取的是对方计算机发送过来的字节
             */
                //启动一个线程与该客户端交互
                ClientHandler clientHandler = new ClientHandler(socket);
                Thread t = new Thread(clientHandler);
                t.start();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }

    private class ClientHandler implements Runnable {
        private Socket socket;
        private String host;

        public ClientHandler(Socket socket) {
            this.socket = socket;
            host = socket.getInetAddress().getHostAddress();
        }

        public void run() {
            PrintWriter pw = null;
            try {
                InputStream in = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(in, "utf-8");
                BufferedReader br = new BufferedReader(isr);

                OutputStream out = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(out, "utf-8");
                BufferedWriter bw = new BufferedWriter(osw);
                pw = new PrintWriter(bw, true);

                synchronized (allOut) {
                    //输出存入allout
                    //1.扩容
//                    allOut = Arrays.copyOf(allOut, allOut.length + 1);
//                    //2.存值
//                    allOut[allOut.length - 1] = pw;
                    allOut.add(pw);
                    sendMessage(host + "上线了，当前在线人数："+allOut.size());
                }

                String message = null;
                while ((message = br.readLine()) != null) {
                    System.out.println(host + "说:" + message);
                    sendMessage(host+"说"+message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                synchronized (allOut) {
                    //处理客户端断开连接
                    //当前客户端的输出流从allout中删除（缩容）
//                    for (int i = 0; i < allOut.length; i++) {
//                        if (allOut[i] == pw){
//                            allOut[i] = allOut[allOut.length-1];
//                            allOut = Arrays.copyOf(allOut,allOut.length-1);
//                            break;
//                        }
//                    }
                    allOut.remove(pw);

                    }
                }
                sendMessage(host+"下线了，当前在线人数："+allOut.size());
                try {
                    socket.close();//与客户端断开连接

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * 广播消息给所有客户端
         * @param message
         */
    private void sendMessage(String message){
        synchronized (allOut) {
//           for (int i = 0; i < allOut.length; i++) {
//               allOut[i].println(message);
//           }
            for (PrintWriter pw : allOut)
            System.out.println(pw);
        }
    }
}
