package socket;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        int[] allOut = {1,4,5,87,9,354,564,654,3,132,44};
        int pw = 4;
        System.out.println(Arrays.toString(allOut));
        //将pw从数组中删除
        for (int i = 0; i < allOut.length; i++) {
            if (pw == allOut[i]){
                allOut[i] = allOut[allOut.length-1];
                allOut = Arrays.copyOf(allOut,allOut.length-1);
                break;
            }
        }
        System.out.println(Arrays.toString(allOut));
    }
}
