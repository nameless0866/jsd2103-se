package socket;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * 聊天室客户端
 */
public class Client {
    /*
        java.net.Socket 套接字
        Socket封装了TCP协议的通讯细节，我们通过它可以与远端计算机建立连接
        并通过他获取两个流（一个输入，一个输出），然后对两个流的数据读写完成
        与远端计算机的数据交互工作。
        我们可以吧Socket想象成是一个电话，电话有一个听筒（输入流），一个麦克风
        （输出流），通过他们就可以与对方交流了。
     */
    private Socket socket;
    public Client()  {
        try {
            System.out.println("正在连接服务端");
            /*
                实例化Socket时要传入两个参数
                参数1：服务端的地址信息
                    可以使IP地址，如果连接本机可以与“localhost”
                参数2：服务端开启的服务端口
                我们通过IP找到网络上的服务端计算机，通过端口链运行在该机器上的服务端应用程序。
                实例化的过程就是连接的过程，如果连接失败会抛出异常：
                java.net.ConnectException：Connection refused：connect
                176.105.17.18 秋实的IP地址
                端口8800
             */
            socket = new Socket("176.105.17.172",8745);
            System.out.println("与服务器建立连接！");
        } catch (IOException e){
            e.printStackTrace();
        }

    }
    public void start(){
        Scanner scan = new Scanner(System.in);
        try {
            ServerHandLer hander = new ServerHandLer();
            Thread t = new Thread(hander);
            t.setDaemon(true);
            t.start();
            /*
                Socket提供了一个方法：
                OutputStream getOutputStream()
                该方法获取的字节输出流写出的字节会通过网络发送给对方计算机
             */
            //低级流，将字节通过网络发送给对方
            OutputStream out = socket.getOutputStream();
            //高级流，负责衔接字节流与字符流，并将学出的字符按指定字符集转字节

            OutputStreamWriter osw = new OutputStreamWriter(out,"utf-8");
            //高级流，负责块写文门数据加速
            BufferedWriter bw = new BufferedWriter(osw);
            //高级流，负责按行写出字符串，自动行刷新
            PrintWriter pw = new PrintWriter(bw,true);


            while (true){
                String line = scan.nextLine();
                if("exit".equalsIgnoreCase(line)){
                    break;
                }
                pw.println(line);
            }
            pw.println("exit");
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public static void main(String[] args) {
        Client client = new Client();
        client.start();
    }
    private class ServerHandLer implements Runnable{
        public void run(){
            try {
                InputStream in = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(in, "utf-8");
                BufferedReader br = new BufferedReader(isr);

                String line;
                while ((line = br.readLine()) != null) {
                    System.out.println(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
