package homework.day03;

import java.io.FileFilter;
import java.io.File;

/**
 * 列出当前目录中所有名字包含s的子项。
 * <p>
 * 使用匿名内部类和lambda两种写法
 * <p>
 * 单词记一记:
 * FileFilter   文件过滤器
 * accept       接受
 *
 * @author Xiloer
 */
public class Test02 {
    public static void main(String[] args) {
        File file = new File(".");
        if (file.isDirectory()) {
            FileFilter f = new FileFilter() {
                public boolean accept(File pathname) {
                    return pathname.getName().indexOf("s") > 0;
                }
            };
            File [] subs = file.listFiles(f);
			for (int i = 0; i < subs.length; i++) {
				String name = subs[i].getName();
				System.out.println("子项"+i+":"+name);
			}
        }
    }
}
