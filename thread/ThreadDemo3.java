package thread;

import upclass.exception.ThrowDemo;

public class ThreadDemo3 {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                System.out.println("天师武艺");
            }
        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                System.out.println("天下无双");
            }
        });
        Thread t3 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                System.out.println("替天行道");
            }
        });
        Thread t4 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                System.out.println("天理昭彰");
            }
        });
        Thread t5 = new Thread(() -> {
            for (int i = 0; i < 10000; i++) {
                System.out.println("帅！~");
            }
        });
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        Runnable r1 = () -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("天师武艺");
            }
        };
        Runnable r2 = () -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("天下无双");
            }
        };
        Runnable r3 = () -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("替天行道");
            }
        };
        Runnable r4 = () -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("真理昭彰");
            }
        };
        Runnable r5 = () -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("帅！！~");
            }
        };
        Thread t11 = new Thread(r1);
        Thread t12 = new Thread(r2);
        Thread t13 = new Thread(r3);
        Thread t14 = new Thread(r4);
        Thread t15 = new Thread(r5);
        t11.start();
        t12.start();
        t13.start();
        t14.start();
        t15.start();
    }
}
