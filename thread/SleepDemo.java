package thread;

/**
 * 线程提供了一个静态方法：
 * static void sleep（long ms）
 * 该方法可以让执行这个方法的线程进入阻塞状态指定毫秒
 *
 */
public class SleepDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了...");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("程序结束了...");
    }
}
