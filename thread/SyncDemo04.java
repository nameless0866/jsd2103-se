package thread;

public class SyncDemo04 {
    public static void main(String[] args) {
        Foo foo = new Foo();
        Thread t1 = new Thread(){
            @Override
            public void run() {
                foo.methodA();
            }
        };
        Thread t2 = new Thread(){
            @Override
            public void run() {
                foo.methodB();
            }
        };
        t1.start();
        t2.start();
    }

}
class Foo{
    public synchronized void methodA(){
        Thread t = Thread.currentThread();
        try {
            System.out.println(t.getName()+"正在执行A方法");
            Thread.sleep(5000);
            System.out.println(t.getName()+"执行完毕");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void methodB(){
        Thread t = Thread.currentThread();
        try {
            System.out.println(t.getName()+"正在执行B方法");
            Thread.sleep(5000);
            System.out.println(t.getName()+"执行完毕");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
