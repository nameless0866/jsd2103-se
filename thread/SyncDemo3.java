package thread;

public class SyncDemo3 {
    public static void main(String[] args) {

    }
}
class Boo{
    public synchronized static void dosome(){
        Thread t = Thread.currentThread();
        try {
            System.out.println(t.getName()+":正在执行dosome方法...");
            Thread.sleep(5000);
            System.out.println(t.getName()+":执行dosome方法完毕");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
