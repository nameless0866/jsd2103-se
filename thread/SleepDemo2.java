package thread;

/**
 * sleep方法要求必须处理终端异常InterruptedException
 * 当一个线程调用sleep方法处于睡眠阻塞的过程中，它的interrupt（）方法被调用时
 * 会中断该阻塞，此时sleep方法会抛出该异常。
 */
public class SleepDemo2 {
    public static void main(String[] args) {
        Thread lin = new Thread(){
            public void run(){
                System.out.println("休息");
                try {
                    Thread.sleep(9999999);
                } catch (InterruptedException e) {
                    System.out.println("惊醒");
                }finally {
                    System.out.println("睡醒");
                }
            }
        };
        Thread huang = new Thread(){
            public void run(){
                System.out.println("咖喱GEIGEI");
                for (int i = 0; i < 5; i++) {
                    System.out.println("蹦蹦蹦");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {}
                }
                System.out.println("欧拉欧拉欧拉欧拉欧拉欧拉");
//                lin.interrupt();
            }
        };
        lin.start();
        huang.start();

    }

}
