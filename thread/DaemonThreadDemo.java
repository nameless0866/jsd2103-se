package thread;

/**
 * 守护线程
 * 守护线程是通过普通县城调用setDaemon（true）设置而转变的。因此守护线程创建上与普通线程无异。
 * 但是结束时机上有一点不同：进程结束。
 * 当一个java进程中的所有普通线程都结束时，该进程就会结束，此时会强制杀死所有正在运行的守护线程
 */
public class DaemonThreadDemo {
    public static void main(String[] args) {
        Thread rose = new Thread(){
            public void run() {
                for (int i = 0 ; i < 5 ; i ++) {
                    System.out.println("rose : let me go");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("rose:aaaaaaaaaaaaaaaaaaa");
                System.out.println("putong");
            }
        };
        Thread jack = new Thread(){
            @Override
            public void run() {
                while(true){
                    System.out.println("jack : you jump i jump");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        rose.start();
        jack.setDaemon(true);
        jack.start();
//        while(true);
    }
}
