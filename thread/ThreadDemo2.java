package thread;

/**
 * 第二种创建线程的方式
 * 实现Runnable接口单独定义线程任务
 */
public class ThreadDemo2 {
    public static void main(String[] args) {
        Runnable r1 = new MyRunnable1();
        Runnable r2 = new MyRunnable2();
        Runnable r3 = new MyRunnable3();
        Runnable r4 = new MyRunnable4();
        Runnable r5 = new MyRunnable5();
        Thread t1 = new Thread(r1);
        Thread t2 = new Thread(r2);
        Thread t3 = new Thread(r3);
        Thread t4 = new Thread(r4);
        Thread t5 = new Thread(r5);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();


    }
}
class MyRunnable1 implements Runnable {
    public void run(){
        for (int i = 0; i < 1000; i++) {
            System.out.println("天师武艺");
        }
    }
}
class MyRunnable2 implements Runnable{
    public void run(){
        for (int i = 0; i < 1000; i++) {
            System.out.println("天下无双");
        }
    }
}
class MyRunnable3 implements Runnable{
    public void run(){
        for (int i = 0; i < 1000; i++) {
            System.out.println("替天行道");
        }
    }
}
class MyRunnable4 implements Runnable{
    public void run(){
        for (int i = 0; i < 1000; i++) {
            System.out.println("天理昭彰");
        }
    }
}
class MyRunnable5 implements Runnable{
    public void run(){
        for (int i = 0; i < 1000; i++) {
            System.out.println("帅~！");
        }
    }
}
