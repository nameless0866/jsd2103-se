package thread;

/**
 * 线程提供了一个join方法，可以协调线程的同步运行。它允许调用该方法的线程等待（阻塞），直到该方法所属线程执行完毕后结束等待（阻塞）继续运行
 *
 * 同步运行：多个线程存在先后顺序
 * 异步运行：多个线程各干各的，线程运行本来就是异步的。
 */
public class JoinDemo {
    public static boolean isFinish = false;
    public static void main(String[] args) {
//        boolean isFinish = false;
        Thread download = new Thread(){
            @Override
            public void run() {
                for (int i = 0; i <= 100; i++) {
                    System.out.println("down"+i+"%");
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {

                    }
                }
                System.out.println("down:下载完毕");
                isFinish = true;
            }
        };
        Thread show = new Thread(){
            @Override
            public void run() {
                try {
                    System.out.println("show:开始显示文字...");
                    Thread.sleep(3000);
                    System.out.println("show:显示文字完毕");
                    download.join();
                    System.out.println("show:开始显示图片...");
                    if(!isFinish){
                        throw new RuntimeException("show:显示图片失败");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        download.start();
        show.start();
    }
}
