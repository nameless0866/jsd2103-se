package thread;

/**
 * 多线程
 * 线程：程序中一个单一的顺序执行流程
 * 多线程：多个单一顺序执行流程“同时”执行
 *
 * 多线程改变了代码的执行方式，从原来的但依顺序执行流程变为多个执行流程“同时”执行
 * 可以让多个代码片段的执行互不打扰
 *
 * 县城之间是并发执行的，并非真正意义上的同时运行
 * 常见线程有两种方式：
 * 1：继承Thread并重写run方法
 *
 */

public class ThreadDemo1 {
    public static void main(String[] args) {
        Thread t1 = new MyThread1();
        Thread t2 = new MyThread2();
        Thread t3 = new MyThread3();
        Thread t4 = new MyThread4();
        Thread t5 = new MyThread5();
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }
}

/**
 * 第一种创建线程的优点：
 * 结构简单，利于匿名内部类形式创建
 *
 * 缺点：
 * 1.由于java是单继承的，这会导致继承了Thread就无法再继承其他类去复用方法
 * 2.定义线程的同时重写了run方法，这等于将线程的任务定义在了这个线程中导致线程只能干这件事，重用性很低
 */
class MyThread1 extends Thread{
    public void run(){
        for (int i = 0; i < 1000; i++) {
            System.out.println("天师武艺");
        }
    }
}
class MyThread2 extends Thread{
    public void run(){
        for (int i = 0; i < 1000; i++) {
            System.out.println("天下无双");
        }
    }
}
class MyThread3 extends Thread{
    public void run(){
        for (int i = 0; i < 1000; i++) {
            System.out.println("替天行道");
        }
    }
}
class MyThread4 extends Thread{
    public void run(){
        for (int i = 0; i < 1000; i++) {
            System.out.println("天理昭彰");
        }
    }
}
class MyThread5 extends Thread{
    public void run(){
        for (int i = 0; i < 1000; i++) {
            System.out.println("帅~！");
        }
    }
}