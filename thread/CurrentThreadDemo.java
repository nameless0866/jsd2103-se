package thread;

/**
 * java中所有的代码都是靠线程运行的，执行main方法的线程称为主线程，这条线程是由虚拟机自动创建的
 * 线程提供了一个方法：
 * static Thread currentThread()
 * 该方法可以获取运行这个方法的线程
 */
public class CurrentThreadDemo {
    public static void main(String[] args) {
        Thread main =Thread.currentThread();
        Thread t = new Thread(){
            public void run(){

            }
        };
        t.start();
        System.out.println("该方法的线程是"+main);
        dosome();
    }
    public static void dosome(){
        Thread t = Thread.currentThread();
        System.out.println("执行dosome方法的线程是："+t);
    }
}
