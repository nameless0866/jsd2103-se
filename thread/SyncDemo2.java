package thread;

/**
 * 有效地缩小同步范围可以在保证安全的情况下
 */
public class SyncDemo2 {
    public static void main(String[] args) {
        Shop shop = new Shop();
        Thread t1 = new Thread(){
            public void run() {
                shop.buy();
            }
        };
        Thread t2 = new Thread(){
            public void run() {
                shop.buy();
            }
        };
        t1.start();
        t2.start();
    }
}
class Shop{
    public void buy(){
        /*
            在方法上使用syn，那么同步监视器对象就是this
         */
        Thread t = Thread.currentThread();
        try {
            System.out.println(t.getName()+"正在挑衣服...");
            Thread.sleep(5000);
//            synchronized (this){
            synchronized (new Object()){
                System.out.println(t.getName()+"正在试衣服...");
                Thread.sleep(5000);
            }
            System.out.println(t.getName()+"结账离开");
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
